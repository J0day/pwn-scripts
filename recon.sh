#!/bin/bash

#---------------------------------------------
# Combination Script of Dir.sh & Recon.sh
#---------------------------------------------

# Colors
	Red=$'\e[1;31m'
	Green=$'\e[1;32m'
	Orange=$'\e[1:33m'
	Blue=$'\e[1;34m'
	Purple=$'\e[1;35m'
	White=$'\e[0m'

# Get Directory Name
	read -p 'Directory Name: ' Dir
	echo""

# Make Directory
	mkdir $Dir

# Move to Directory
	cd $Dir

# Create Markdown file
	subl Notes.md -b

# Format the file
	echo 'Notes
------------------------------------------------------
	Enumeration
		-

	Post exploit
		-

	Credentials
		-

	' >> Notes.md

# Get IP Address
	read -p 'Enter the IP Address: '$Green IP

# Send IP address to notes
	echo IP Address: $IP >> Notes.md
	echo"" $Blue

# Artwork
	echo '
     /  /\         /  /\         /  /\         /  /\         /  /\
    /  /::\       /  /::\       /  /::\       /  /::\       /  /::|
   /  /:/\:\     /  /:/\:\     /  /:/\:\     /  /:/\:\     /  /:|:|
  /  /::\ \:\   /  /::\ \:\   /  /:/  \:\   /  /:/  \:\   /  /:/|:|__
 /__/:/\:\_\:\ /__/:/\:\ \:\ /__/:/ \  \:\ /__/:/ \__\:\ /__/:/ |:| /\
 \__\/~|::\/:/ \  \:\ \:\_\/ \  \:\  \__\/ \  \:\ /  /:/ \__\/  |:|/:/
    |  |:|::/   \  \:\ \:\    \  \:\        \  \:\  /:/      |  |:/:/
    |  |:|\/     \  \:\_\/     \  \:\        \  \:\/:/       |__|::/
    |__|:|~       \  \:\        \  \:\        \  \::/        /__/:/
     \__\|         \__\/         \__\/         \__\/         \__\/
	'

# Nmap Portion
	echo $Purple
	echo "----------------------- NMAP FAST Scan --------------------------"
	echo $White
	nmap -sV -sC -p- $IP --open --stats-every=5 -oN fast.txt
	echo""

	echo $Purple
	echo "----------------------- NMAP  TCP Scan --------------------------"
	echo $White
	nmap -A -T4 -p- -Pn  $IP --script-args=unsafe=1 -oN nmap.txt
	echo""

	echo "----------------------- NMAP UDP Scan --------------------------"
        echo $White
        sudo nmap -sU --top-ports=1000 $IP -oN udp.txt
        echo""


# Feroxbuster Portion
	echo $Red
	echo "----------------------- Directory Scan --------------------------"
	echo $White

# Attention makesure you change wordlist location 

	echo "----------------------COMMON.TXT----------------------------------" > dir.txt && echo "" >> dir.txt &&
	ffuf -w /home/kali/wordlists/SecLists/Discovery/Web-Content/common.txt -e html,txt,php,js,docx,zip,aspx,cgi,sh -t 25 -se -c -ac -mc 200,204,301,307,400,401,403,405,302 -u http://$IP/FUZZ  -c -ac -t 25 >> dir.txt && echo "" >> dir.txt && echo "--------- MEDIUM.TXT-------------------------------------" >> dir.txt &&
	ffuf -w /home/kali/wordlists/SecLists/Discovery/Web-Content/directory-list-2.3-medium.txt -e html,txt,php,js,docx,zip,aspx,cgi,sh -t 25 -c -ac -se -mc 200,204,301,307,400,401,403,405,302 -u http://$IP/FUZZ  >> dir.txt && echo "" >> dir.txt && echo "--------- BIG.TXT -------------------------------------" >> dir.txt &&
	ffuf -w /home/kali/wordlists/SecLists/Discovery/Web-Content/big.txt -e html,txt,php,js,docx,zip,aspx,cgi,sh -t 25 -c -se -ac -mc 200,204,301,307,400,401,403,405,302 -u http://$IP/FUZZ >> dir.txt
	echo ""

	echo $Purple
	echo "----------------------- Nmap Vuln Scan --------------------------"
	echo $White
	nmap --script vuln -Pn $IP -oN Nmapvuln.txt
	echo""

 #Nikto Portion
	echo $Orange
	echo "----------------------- Nikto Scan --------------------------"
	echo $White
	nikto -h $IP -output nikto.txt
